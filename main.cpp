#include <iostream>
#include "model/DataBase.h"
#include "model/DataHandler.h"
#include "modules/CreateDB.cpp"
#include "app/App.h"

using namespace std;

int main() {
    DataBase db("data.db");
    App app(db);
    unique_ptr<DataHandler> dataHandler; // Используем умный указатель

    if (app.UpConnection()) {
        cout << "Соединение с базой данных установлено." << endl;
        dataHandler = make_unique<DataHandler>(app.UpdateHandler()); // Создаем объект и передаем его в умный указатель

        CreateDB createDB(*dataHandler);
        createDB.create();
    } else {
        cerr << "Ошибка при открытии соединения с базой данных." << std::endl;
        return 1;
    }
    App::start(*dataHandler);

    // Добавление записи в таблицу.
    std::string insertSQL = "INSERT INTO users (name, login, password, role) VALUES ('John', 'John', '1111', 0)";

    if (dataHandler->executeNonQuery(insertSQL)) {
        std::cout << "Запись добавлена в таблицу." << std::endl;
    } else {
        std::cerr << "Ошибка при добавлении записи." << std::endl;
        return 1;
    }

    // Чтение данных из таблицы и вывод на консоль.
    std::string selectQuery = "SELECT * FROM users;";
    std::vector<std::vector<std::string>> result = dataHandler->executeQuery(selectQuery);

    std::cout << "Содержимое таблицы:" << std::endl;
    for (const auto& row : result) {
        for (const std::string& value : row) {
            std::cout << value << "\t";
        }
        std::cout << std::endl;
    }

    // Удаление записи из таблицы.
    std::string deleteSQL = "DELETE FROM users WHERE name = 'John';";
    if (dataHandler->executeNonQuery(deleteSQL)) {
        std::cout << "Запись удалена из таблицы." << std::endl;
    } else {
        std::cerr << "Ошибка при удалении записи." << std::endl;
        return 1;
    }


    return 0;
}
