//
// Created by Denis Ryzhkov on 25.10.2023.
//

#ifndef CW1_APP_H
#define CW1_APP_H


#include <iostream>
#include "../model/DataBase.h"
#include "../model/DataHandler.h"
#include "../controller/ManageUsers.h"
#include "appView/userMenu.cpp"
#include "modules/UserManager.cpp"
#include "modules/SubjectManager.cpp"
#include "../controller/ManageTest.h"
#include "../controller/ManageSubject.h"
#include "../modules/export_import.h"


class App : public UserMenu {
    DataBase &db;

public:
    App(DataBase &db);
    bool UpConnection();
    bool DownConnection();
    DataHandler UpdateHandler();
    static void start(DataHandler &dbHandler);
    static int adminMenuManager(User &user, ManageUsers &usersManager, ManageTest &testsManager, ManageSubject &subjectManager, void (*print)(const std::string&));

    static int showUserManageMenu(User &user, ManageUsers &usersManager, void (*print)(const std::string &));

    static int showTestManageTests( ManageTest &testsManager, ManageSubject &subjectManager, void (*print)(const std::string &));

    static int ShowSubjectDetailMenu(Subject &subject, ManageSubject &subjectManager, void (*print)(const std::string &));

    static int
    showTestDetailMenu(Test &test, Subject &subject, ManageTest &testManager, void (*print)(const std::string &));
};


#endif //CW1_APP_H
