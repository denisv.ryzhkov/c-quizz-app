//
// Created by Denis Ryzhkov on 25.10.2023.
//


#include "App.h"




// Создаем объект класса Database для управления соединением с базой данных.
App::App(DataBase &db)  : db(db) {};

bool App::UpConnection() {
    // Открываем соединение с базой данных.
    if (db.open()) {
        return true;
    } else {
        std::cerr << "Ошибка при открытии соединения с базой данных." << std::endl;
        return false;
    }
}

bool App::DownConnection() {
    // Закрываем соединение с базой данных.
    db.close();
    return true;
}

DataHandler App::UpdateHandler() {
        DataHandler dataHandler(db);
        return dataHandler;

}

void App::start(DataHandler &dbHandler) {
    User CurrentUser;
    int userChoice = 0;
    bool MainLoop = true;
    bool firstStartLoop = true;
    bool loginLoop = true;
    //первый старт или нет
    ManageUsers usersManager(dbHandler);
    ManageTest testsManager(dbHandler);
    ManageSubject subjectsManager(dbHandler);

    int userCount = usersManager.GetUsersCount();
    if (userCount <= 0 ) {
        while (firstStartLoop) {
            userChoice = showMenuStartMenu();
            switch (userChoice) {
                case 1:
                    CurrentUser = UserManager::createNewUser(CurrentUser, &print, 1);
                    if (usersManager.createUser(CurrentUser)) {
                        print(CurrentUser.name + ". Вы успешно зарегистрировались:\n");
                        firstStartLoop = false;
                        break;
                    }
                case 0:
                    exit(0);
                default:
                    std::cout << "Введите корректное число" << std::endl;
                    continue;
            }
        }
    }
    //теперь когда есть хоть один пользователь можем войти
    while (loginLoop) {
        userChoice = showMenu();
        switch (userChoice) {
            case 1: {
                std::string login, password;
                print("Введите Ваш логин: ");
                std::cin >> login;
                print("Введите Ваш пароль: ");
                std::cin >> password;
                print("\n");
                CurrentUser = usersManager.login(login, password);
                if (CurrentUser.isAuthenticated) {
                    if (CurrentUser.role == 1) {
                        userChoice = adminMenuManager(CurrentUser, usersManager, testsManager, subjectsManager, &print);
                    } else {
                        userChoice = UserMenu::showMenuUserMenu(CurrentUser.name);
                    }
                    loginLoop = false;
                } else {
                    print("Логин или пароль неверны\n");
                    userChoice = UserMenu::showMenu();
                    continue;
                }
            }
                break;

            case 2:
                CurrentUser = UserManager::createNewUser(CurrentUser, &print, 0);
                if (usersManager.createUser(CurrentUser)) {
                    print(CurrentUser.name + ". Вы успешно зарегистрировались:\n");
                    continue;
                }
                break;
            case 0:
                exit(0);
            default:
                std::cout << "Введите корректное число" << std::endl;
                continue;
        }
    }
    if(userChoice == 9)
        App::start(dbHandler);
    else
        exit(0);
}


int App::adminMenuManager(User &user, ManageUsers &usersManager, ManageTest &testsManager, ManageSubject &subjectManager, void (*print)(const std::string&))
{
    bool menuLoop = true;
    int returnUserChoice = 0;
    while (menuLoop) {
        int userChoice = showMenuAdminMenu(user.name);
        switch (userChoice) {
            case 1: {
                bool caseLoop = true;
                while (caseLoop) {
                    print("Текущий Login пользователя: " + user.name + ": " +
                          user.login + "\nВведите новый Login или Enter чтобы оставить: ");
                    std::string newLogin;
                    std::cin >> newLogin;
                    if (!newLogin.empty()) {
                        if (Validation::isValidLogin(newLogin)) {
                            user.login = newLogin;
                            if (usersManager.editUser(user)) {
                                print(user.name + ". Вы успешно изменили свой Login на:" + user.login + "\n");
                                caseLoop = false;
                            }
                        } else {
                            print("\nЛогин должен состоять из более 3 букв\n");
                            continue;
                        }
                    }
                }
            }
                break;
            case 2: {
                bool caseLoop = true;
                while (caseLoop) {
                    print("Смена пароля для пользователя: " + user.name
                    + "\nВведите новый Пароль или Enter чтобы оставить: ");
                    std::string password;
                    std::cin >> password;
                    if (!password.empty()) {
                        if (Validation::isValidPassword(password)) {
                            user.password = password;
                            if (usersManager.editUser(user)) {
                                print(user.name + ". Вы успешно изменили свой пароль!\n");
                                caseLoop = false;
                            }
                        } else {
                            print("\nПароль должен состоять из более 6 букв и цифр и иметь одну заглавную\n");
                            continue;
                        }
                    }
                }
            }
                break;
            case 3: {
                if(App::showUserManageMenu(user, usersManager, *print) == 9)
                    continue;
            }break;
            case 5:
                if(App::showTestManageTests(testsManager, subjectManager, *print) == 9)
                    continue;
                break;
            case 9:
                menuLoop = false;
                returnUserChoice = 9;
                break;
                case 0:
                    exit(0);
            default:
                returnUserChoice = 9;
                break;
        }
    }

    return returnUserChoice;
}

int App::showTestManageTests(ManageTest &testsManager, ManageSubject &subjectManager, void (*print)(const std::string &)) {
    Test test;
    Subject subject;
    int userChoice = showMenuManageTestsSubject();
    switch (userChoice) {
        case 1:
            userChoice = App::showTestDetailMenu(test, subject, testsManager, *print);
            break;
        case 2:
            userChoice = App::ShowSubjectDetailMenu(subject, subjectManager, *print);
            break;
    }

    return userChoice;
}
int App::showTestDetailMenu(Test &test, Subject &subject, ManageTest &testManager,
                            void (*print)(const std::string &)) {

    int returnUserChoice = 0;
    bool menuLoop = true;
    while (menuLoop) {
        int userChoise = showMenuManageTest(testManager, )
    }
    return returnUserChoice;
}


int App::ShowSubjectDetailMenu(Subject &subject, ManageSubject &subjectManager, void (*print)(const std::string &)) {
    int returnUserChoice = 0;
    bool menuLoop = true;
    while (menuLoop) {
       int userChoise = showMenuManageSubjects(subjectManager);
        switch (userChoise) {
            case 1: {
                bool caseLoop = true;
                while (caseLoop) {
                    subject = SubjectManager::CreateNewSubject(subject, *print);
                    if (subjectManager.addSubject(subject.subjectName)) {
                        print("Предмет " + subject.subjectName + ". успешно добавлен:\n");
                        caseLoop = false;
                    }
                }
            }
                break;
            case 2: {
                bool caseLoop = true;
                while (caseLoop) {
                    subject = SubjectManager::EditSubject(subject, *print);
                    if (subjectManager.editSubject(subject.subjectID, subject.subjectName)) {
                        print("Предмет " + subject.subjectName + ". успешно обновлен:\n");
                        caseLoop = false;
                    }
                }
            }
                break;
            case 3: {
                bool caseLoop = true;
                while (caseLoop) {
                    subject = SubjectManager::DeleteSubject(subject, subjectManager, *print);
                    if (subjectManager.deleteSubject(subject.subjectID)) {
                        print("Предмет " + subject.subjectName + ". успешно удален:\n");
                        caseLoop = false;
                    }
                }
            }
            case 4:{
                std::string JsonPath = export_import::exportToJson(subjectManager);
                std::string HTMLPath = export_import::exportToHTML(subjectManager);
                print("Даныне успешно экспортированы в файлы:\nJSON: "+JsonPath+"\nHTML: "+HTMLPath+"\n");
            }
        }
        if (userChoise == 9)
        {
            returnUserChoice = userChoise;
            menuLoop = false;
        }
    }
    return returnUserChoice;
}

int App::showUserManageMenu(User &user, ManageUsers &usersManager, void (*print)(const std::string&)){
    int userChoise = showMenuManageUsers(usersManager);
    User manageUser;
    switch (userChoise) {
        case 1:{
            bool caseLoop = true;
            while (caseLoop) {
                manageUser = UserManager::createNewUser(manageUser, *print, 0);
                if (usersManager.createUser(manageUser)) {
                    print("Пользователь "+ manageUser.name + ". успешно добавлен:\n");
                    caseLoop = false;
                }
            }
        }
            break;
        case 2:{
            bool caseLoop = true;
            while (caseLoop) {
                print("Введите ID пользователя для редактирования: ");
                int id;
                std::cin >> id;
                manageUser = usersManager.getUserByID(id);
                manageUser = UserManager::editUser(manageUser, *print, 0);

                if (usersManager.editUser(manageUser)) {
                    print(user.name + ". Вы успешно отредактировали пользователя с ID:" + std::to_string(id) + "\n");
                    caseLoop = false;
                } else {
                    print("\nID пользователя не найден\n");
                    continue;
                }
            }
        }
        case 3:{
            bool caseLoop = true;
            while (caseLoop) {
                print("Введите ID пользователя для удаления: ");
                int id;
                std::cin >> id;
                if (usersManager.deleteUser(id)) {
                    print(user.name + ". Вы успешно удалили пользователя с ID:" + std::to_string(id) + "\n");
                    caseLoop = false;
                } else {
                    print("\nID пользователя не найден\n");
                    continue;
                }
            }
        }
    }
    return userChoise;
}



//        //Запускаем цикл выполнения программы.
//        while (MainLoop) {
//            switch (userChoice) {
//                case 1:
//                    user.AddUser();
//                    break;
//                case 2:
//                    user.DeleteUser();
//                    break;
//                case 0:
//                    MainLoop = false;
//                    break;
//                default:
//                    std::cout << "Введите корректное число" << std::endl;
//                    break;
//            }
//            switch (command) {
//                case 1:
//                    dbHandler.addRecord();
//                    break;
//                case 2:
//                    dbHandler.updateRecord();
//                    break;
//                case 3:
//                    dbHandler.deleteRecord();
//                    break;
//                case 4:
//                    started = false;
//                    break;
//                default:
//                    std::cout << "Неверная команда!" << std::endl;
//                    break;
//            }
//        }






