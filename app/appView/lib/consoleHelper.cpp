//
// Created by Denis Ryzhkov on 28.10.2023.
//
#include "consoleHelper.h"
#include <cstdlib>

void ConsoleHelper::clearScreen() {
#ifdef _WIN32
    system("cls");
#else
    system("clear");
#endif
}