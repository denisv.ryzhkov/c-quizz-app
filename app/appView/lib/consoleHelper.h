//
// Created by Denis Ryzhkov on 28.10.2023.
//

#pragma once

class ConsoleHelper {
public:
    static void clearScreen();
};


#define CLEAR_SCREEN ConsoleHelper::clearScreen()