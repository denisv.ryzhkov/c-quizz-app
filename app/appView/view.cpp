//
// Created by Denis Ryzhkov on 28.10.2023.
//
#include <iostream>
#include <vector>
#include <iomanip>
#include <locale>
#include "../../controller/ManageUsers.h"
#include "../../controller/ManageTest.h"
#include "../../controller/ManageSubject.h"

#include "lib/consoleHelper.h"

class Viewer
{
public:
    static void printUserTable(const std::vector<User>& users) {
        CLEAR_SCREEN;
        std::string title = "USERS";
        int width = 89;
        int padding = (width - title.length()) / 2;
        // Заголовок таблицы
        std::cout << std::setfill('*') << std::setw(90) << "*" << std::setfill(' ') << std::endl;
        std::cout << "| " << std::setw(padding) << "" << title << std::setw(padding) << " |" << std::endl;
        std::cout << std::setfill('*') << std::setw(90) << "*" << std::setfill(' ') << std::endl;

        std::cout << std::setfill('=') << std::setw(90) << "=" << std::setfill(' ') << std::endl;
        std::cout << "| " << std::left << std::setw(3) << "#";
        std::cout << " | " << std::left << std::setw(12) << "Login";
        std::cout << " | " << std::left << std::setw(17) << "Name";
        std::cout << " | " << std::left << std::setw(28) << "Address";
        std::cout << " | " << std::left <<std::setw(15)<< "Phone" << " |" << std::endl;

        // Разделитель заголовка и данных
        std::cout << std::setfill('=') << std::setw(90) << "=" << std::setfill(' ') << std::endl;

        // Вывод данных о пользователях
        for (const User& user : users) {
            std::cout << "| " << std::left << std::setw(3) << user.userID;
            std::cout << " | " << std::left << std::setw(12) << user.login;
            std::cout << " | " << std::left << std::setw((is_ascii(user.name))? 17 : 24) << user.name;
            std::cout << " | " << std::left << std::setw((is_ascii(user.address))? 28: 38 ) << user.address;
            std::cout << " | " << std::left <<std::setw(15)<< user.phone << " |" << std::endl;
            std::cout << std::setfill('-') << std::setw(90) << "-" << std::setfill(' ') << std::endl;
        }
        std::cout << std::setfill('=') << std::setw(90) << "=" << std::setfill(' ') << std::endl;
    }


    static void printTestTable(std::vector<Test> vector1) {

    }

    static void printSubjectTable(std::vector<Subject> &subjects, ManageSubject &manageSubject) {
        CLEAR_SCREEN;

        std::string title = "SUBJECT";
        int width = 89;
        int padding = (width - title.length()) / 2;
        std::string name = "Subject name";

        // Заголовок таблицы
        std::cout << std::setfill('*') << std::setw(90) << "*" << std::setfill(' ') << std::endl;
        std::cout << "| " << std::setw(padding) << "" << title << std::setw(padding) << " |" << std::endl;
        std::cout << std::setfill('*') << std::setw(90) << "*" << std::setfill(' ') << std::endl;

        std::cout << std::setfill('=') << std::setw(90) << "=" << std::setfill(' ') << std::endl;

        std::cout << "| " << std::left << std::setw(3) << "#";
        std::cout << " | " << std::left << std::setw(int(74- name.size())) << name;
        std::cout << " | " << std::left <<std::setw(15)<< "Test Count" << " |" << std::endl;

        // Разделитель заголовка и данных
        std::cout << std::setfill('=') << std::setw(90) << "=" << std::setfill(' ') << std::endl;

        for (const Subject& subject : subjects) {
            std::cout << "| " << std::left << std::setw(3) << subject.subjectID;
            std::cout << " | " << std::left << std::setw((is_ascii(subject.subjectName)? 62: 72) ) << subject.subjectName;
            std::cout << " | " << std::left << std::setw(15) << manageSubject.getTestCountBySubject(subject.subjectName) << " |" << std::endl;
            std::cout << std::setfill('-') << std::setw(90) << "-" << std::setfill(' ') << std::endl;
        }
        std::cout << std::setfill('=') << std::setw(90) << "=" << std::setfill(' ') << std::endl;


    }
private:
    static bool is_ascii(const std::string& str) {
        for (char c: str)
            if (c < 0) {
                return false; // Если найден многобайтовый символ, вернуть false
            }

        return true; // Если все символы однобайтовые, вернуть true
    }
};