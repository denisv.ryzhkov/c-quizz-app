//
// Created by Denis Ryzhkov on 25.10.2023.
//

#include <iostream>
#include <iomanip>
#include "view.cpp"


class UserMenu{
    public:

    static void print(const std::string& data){
        std::cout << data;
    }

    static int readNumber()
    {
        int value;
        while (true) {
            if (std::cin >> value) {
                return value;
            } else {
                std::cout << "Неверный ввод, пожалуйста повторите попытку" << std::endl;
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }
    }

    static int showMenuStartMenu()
    {
        bool flag = true;
        int command;
        std::cout << "Welcome to the QuizApp, it's a first start:" << std::endl;
        while (flag) {
            CLEAR_SCREEN;
            std::cout << "Введите команду:" << std::endl;
            std::cout << "1.Регистрация администратора" << std::endl;
            std::cout << "0.Выход" << std::endl;
            command = readNumber();
            flag = false;
        }
        return command;
    }
    static int showMenu(){
        bool flag = true;
        int command;
        std::cout << "Welcome to the QuizApp:" << std::endl;
        while (flag) {
            CLEAR_SCREEN;
            std::cout << "Введите команду:" << std::endl;
            std::cout << "1.Вход" << std::endl;
            std::cout << "2.Регистрация" << std::endl;
            std::cout << "0.Выход" << std::endl;

            command = readNumber();
            flag = false;
        }
        return command;
    }
    static int showMenuAdminMenu(const std::string &userName){
        bool flag = true;
        int command;
        std::cout << "Welcome: "<< userName<<" to Admin menu\nPlease select menu option" << std::endl;
        while (flag) {
            CLEAR_SCREEN;
            std::cout << "Введите команду:" << std::endl;
            std::cout << "1.Сменить логин" << std::endl;
            std::cout << "2.Сменить пароль" << std::endl;
            std::cout << "3.Управление пользователями" << std::endl;
            std::cout << "4.Статистика" << std::endl;
            std::cout << "5.Управление тестами" << std::endl;
            std::cout << "9.Вернуться к авторизации" << std::endl;
            std::cout << "0.Выход" << std::endl;
            command = readNumber();
            flag = false;
        }
        return command;
    }
    static int showMenuManageUsers(ManageUsers &usersManager){
        bool flag = true;
        int command;

        while (flag) {
            CLEAR_SCREEN;
            Viewer::printUserTable(usersManager.getAllUsers());
            std::cout << std::left << std::setw(13) << "1.ADD";
            std::cout << std::left << std::setw(13) << "2.EDIT";
            std::cout << std::left << std::setw(13) << "3.DELETE";
            std::cout << std::left << std::setw(25  ) << "3.EXPORT (HTML/JSON)";
            std::cout << std::left << std::setw(13) <<"9.RETURN";
            std::cout << std::left << "0.EXIT" << std::endl;
            command = readNumber();
            flag = false;
        }
        return command;
    }

    static int showMenuManageTestsSubject(){
        bool flag = true;
        int command;
        std::cout << "Управление тестами" << std::endl;
        while (flag) {
            CLEAR_SCREEN;
            std::cout << "Введите команду:" << std::endl;
            std::cout << "1.Управление тестами" << std::endl;
            std::cout << "2.Управление Предметами" << std::endl;
            std::cout << "2.Добавить предмет" << std::endl;
            std::cout << "3.Добавить тест" << std::endl;
            std::cout << "3.Удалить тест" << std::endl;
            std::cout << "3.Экспортировать тест (HTML/JSON)" << std::endl;
            std::cout << "3.Экспортировать все тесты (HTML/JSON)" << std::endl;
            std::cout << "3.Импортировать тест (JSON)" << std::endl;
            std::cout << "9.Вернуться назад" << std::endl;
            std::cout << "0.Выход" << std::endl;
            command = readNumber();
            flag = false;
        }
        return command;
    }
    static int showMenuManageSubjects(ManageSubject &subjectManager){
        bool flag = true;
        int command;

        while (flag) {
            CLEAR_SCREEN;
            std::vector<Subject> subjects = subjectManager.getAllSubjects();
            Viewer::printSubjectTable(subjects, subjectManager);
            std::cout << std::left << std::setw(13) << "0.DETAIL";
            std::cout << std::left << std::setw(13) << "1.ADD";
            std::cout << std::left << std::setw(13) << "2.EDIT";
            std::cout << std::left << std::setw(13) << "3.DELETE";
            std::cout << std::left << std::setw(20) << "4.EXPORT (HTML/JSON)";
            std::cout << std::left << std::setw(13) << "9.RETURN";
            command = readNumber();
            flag = false;
        }
        return command;
    }

    static int showMenuManageTest(ManageTest &testManager, std::string &subjectName){
        bool flag = true;
        int command;

        while (flag) {
            CLEAR_SCREEN;
            Viewer::printTestTable(testManager.getTestsBySubject(subjectName));
            std::cout << std::left << std::setw(13) << "1.ADD";
            std::cout << std::left << std::setw(13) << "2.EDIT";
            std::cout << std::left << std::setw(13) << "3.DELETE";
            std::cout << std::left << std::setw(15) << "4.EXPORT ALL";
            std::cout << std::left << std::setw(15) << "5.EXPORT ONE";
            std::cout << std::left << std::setw(13) <<"9.RETURN";
            std::cout << std::left << "0.EXIT" << std::endl;
            command = readNumber();
            flag = false;
        }
        return command;
    }

    static int showMenuStatistics(){
        bool flag = true;
        int command;
        std::cout << "Статистика" << std::endl;
        while (flag) {
            CLEAR_SCREEN;
            std::cout << "Введите команду:" << std::endl;
            std::cout << "1.Статистика по пользователям" << std::endl;
            std::cout << "2.Статистика по предметам" << std::endl;
            std::cout << "9.Вернуться назад" << std::endl;
            std::cout << "0.Выход" << std::endl;
            command = readNumber();
            flag = false;
        }
        return command;
    }
    static int showMenuUserMenu(const std::string &userName){
        std::cout << "Welcome: "<< userName<<" to Admin menu\nPlease select menu option" << std::endl;
        return 0;
    }


};