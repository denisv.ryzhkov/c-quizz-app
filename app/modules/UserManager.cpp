//
// Created by Denis Ryzhkov on 25.10.2023.
//
#include <iostream>
#include "../../controller/ManageUsers.h"
#include "../../modules/Validation.cpp"

class UserManager : private  Validation{
public:
    static User createNewUser(User &user,  void (*print)(const std::string&),  int isAdmin = 0){
        std::string temp;
        while (true) {
            if(user.name.empty()) {
                print("Введите ФИО: ");
                std::getline(std::cin, temp);
                if (isValidName(temp))
                    user.name = temp;
                else {
                    print("\nФИО должно состоять из букв, и пробелов\n");
                    continue;
                }
            }
            if(user.address.empty()) {
                print("\nВведите адрес: ");
                std::getline(std::cin, temp);
                if (isValidLogin(temp))
                    user.address = temp;
                else {
                    print("\nАдрес должен состоять из более 3 букв\n");
                    continue;
                }
            }
            if(user.phone.empty()) {
                print("\nВведите телефон: ");
                std::getline(std::cin, temp);
                if (isPhoneNumberValid(temp))
                    user.phone = temp;
                else {
                    print("\nТелефон должен состоять не менее чем из  10 цифр\n");
                    continue;
                }
            }
            if(user.login.empty()) {
                print("\nВведите логин: ");
                std::cin >> temp;
                if (isValidLogin(temp))
                    user.login = temp;
                else {
                    print("\nЛогин должен состоять из более 3 букв\n");
                    continue;
                }
            }
            if(user.password.empty()) {
                print("\nВведите пароль: ");
                std::cin >> temp;
                if (isValidPassword(temp))
                    user.password = temp;
                else {
                    print("\nПароль должен состоять из более 6 букв и цифр и иметь одну заглавную\n");
                    continue;
                }
            }
            user.role = isAdmin;
            return user;
        }
    }

    static User editUser(User &user,  void (*print)(const std::string&),  int isAdmin = 0){
        std::string temp;
        while (true) {
            print("\n1."+user.name);
            print("2."+ user.address+ "\n");
            print("3."+ user.phone+ "\n");
            print("4."+ user.login+ "\n");
            int choice = 0;
            std::cin >> choice;
            switch (choice) {
                case 1:
                    print("\nВведите новые ФИО: ");
                    std::getline(std::cin, temp);
                    if (isValidName(temp))
                        user.name = temp;
                    else {
                        print("\nФИО должно состоять из букв, и пробелов\n");
                        continue;
                    }
                    break;
                case 2:
                    print("\nВведите новый адрес: ");
                    std::getline(std::cin, temp);
                    if (isValidLogin(temp))
                        user.address = temp;
                    else {
                        print("\nАдрес должен состоять из более 3 букв\n");
                        continue;
                    }
                    break;
                case 3:
                    print("\nВведите новый телефон: ");
                    std::getline(std::cin, temp);
                    if (isPhoneNumberValid(temp))
                        user.phone = temp;
                    else {
                        print("\nТелефон должен состоять не менее чем из  10 цифр\n");
                        continue;
                    }
                    break;
                case 4:
                    print("\nВведите новый логин: ");
                    std::cin >> temp;
                    if (isValidLogin(temp))
                        user.login = temp;
                    else {
                        print("\nЛогин должен состоять из более 3 букв\n");
                        continue;
                    }
                    break;

            }
            user.role = isAdmin;
            return user;
        }
    }

};