//
// Created by Denis Ryzhkov on 06.11.2023.
//
#include <iostream>
#include "../../controller/ManageSubject.h"
#include "../../modules/Validation.cpp"

class SubjectManager: private  Validation
{
public:
    static Subject CreateNewSubject(Subject &subject,  void (*print)(const std::string&))
    {
        std::string name;
        while(true) {
            if (subject.subjectName.empty()) {
                print("Введите название предмета: ");
                std::getline(std::cin, name);
                if (isValidName(name))
                    subject.subjectName = name;
                else {
                    print("\nНазвание предмета должно состоять из более 3 букв\n");
                    continue;
                }
            }
            return subject;
        }

    }

    static Subject EditSubject(Subject &subject,  void (*print)(const std::string&)) {
        std::string name;
        while (true) {
            print("Редактирование " + subject.subjectName + "\n");
            print("Введите новое название предмета: ");
            std::getline(std::cin, name);
            if (isValidName(name))
                subject.subjectName = name;
            else {
                print("\nНазвание предмета должно состоять из более 3 букв\n");
                continue;
            }

            return subject;
        }
    }


    static Subject DeleteSubject(Subject &subject, ManageSubject &subjectManager, void (*print)(const std::string &)) {
        while (true) {
            print("Удаление " + subject.subjectName + "\n");
            print("Введите ID предмета для удаления: ");
            std::basic_string<char> ID;
            int choice = 0;
            std::cin >> choice;
            subject = subjectManager.getSubjectByID(choice);
            if (!subject.subjectName.empty())
                return subject;
            else
            {
                print("Введите коректный ID предмета для удаления: ");
                continue;
            }
        }
    }
};