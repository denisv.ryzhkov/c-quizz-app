//
// Created by Denis Ryzhkov on 25.10.2023.
//
#pragma once
#include <ctime>
#include <cctype>
#include <string>

class Validation {

    static bool isNumeric(const std::string &str, size_t startPos) {
        for (size_t i = startPos; i < str.length(); ++i) {
            if (!std::isdigit(str[i])) {
                return false;
            }
        }
        return true;
    }

public:
    // Проверка даты рождения (формат, не менее 18 лет не более 80)
    static bool isValidDateOfBirth(const std::string& dateOfBirth) {
        // Проверка формата даты (год-месяц-день)
        if (dateOfBirth.length() != 10) {
            return false;
        }

        struct tm birthTime = {0};
        if (strptime(dateOfBirth.c_str(), "%Y-%m-%d", &birthTime) == nullptr) {
            return false;
        }

        // Получение текущей даты и вычисление возраста
        time_t now = time(nullptr);
        struct tm currentTime = {0};
        localtime_r(&now, &currentTime);
        int age = currentTime.tm_year + 1900 - birthTime.tm_year;

        // Проверка возраста (18 <= age <= 80)
        return (age >= 18 && age <= 80);
    }

    // Проверка логина (только буквы и не менее 3 символов)
    static bool isValidLogin(const std::string& login) {
        if (login.length() < 3) {
            return false;
        }

//        for (char ch: login)
//            if (!isalpha(ch))
//                return false;

        return true;
    }


    // Проверка логина с обработкой украинского апострофа
    static bool isValidName(const std::string& login, char replacement = '\'') {
        std::string modifiedLogin = login;
        for (char& ch : modifiedLogin) {
            if (ch == -103) { // Украинский апостроф (’)
                ch = replacement;
            }
        }
        return isValidLogin(modifiedLogin);
    }

    // Проверка пароля (только буквы и цифры, не менее 6 знаков, и обязательно одна буква большая)
    static bool isValidPassword(const std::string &password) {
        if (password.length() < 6) {
            return false;
        }

        bool hasDigit = false;
        bool hasUppercase = false;

        for (char ch : password) {
            if (isalpha(ch)) {
                if (isupper(ch)) {
                    hasUppercase = true;
                }
            } else if (isdigit(ch)) {
                hasDigit = true;
            } else {
                // Найден специальный символ, пароль недопустим
                return false;
            }
        }

        // Проверяем, что пароль содержит хотя бы одну цифру и одну большую букву
        return hasDigit && hasUppercase;
    }

    static bool isPhoneNumberValid(const std::string &phoneNumber) {
        // Проверяем, что строка не пуста и её длина находится в заданных пределах.
        if (phoneNumber.empty() || phoneNumber.size() < 10 || phoneNumber.size() > 15) {
            return false;
        }

        // Проверяем, что первый символ может быть "+" (не обязательно).
        if (phoneNumber[0] == '+') {
            // Если "+" присутствует, начнём проверку с первого непустого символа.
            return isNumeric(phoneNumber, 1);
        } else {
            // В противном случае, начнём проверку с начального символа.
            return isNumeric(phoneNumber, 0);
        }
    }
};