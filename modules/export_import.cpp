//
// Created by Denis Ryzhkov on 08.11.2023.
//

#include "export_import.h"

#ifdef _WIN32 // Определение для Windows
#include <windows.h>
std::string getDesktopPath() {
    TCHAR username[UNLEN + 1];
    DWORD usernameLen = UNLEN + 1;
    if (GetUserName(username, &usernameLen)) {
        return "C:\\Users\\" + std::string(username) + "\\Desktop\\";
    }
    return "";
}
#elif __APPLE__ // Определение для macOS
#include <pwd.h>
#include <unistd.h>
std::string getDesktopPath() {
    struct passwd *pw = getpwuid(getuid());
    std::string username = pw->pw_name;
    return "/Users/" + username + "/Desktop/";
}
#else
#include <pwd.h>
#include <unistd.h>
std::string getDesktopPath() {
    struct passwd *pw = getpwuid(getuid());
    std::string username = pw->pw_name;
    return "/home/" + username + "/Desktop/";
}
#endif

// Функция для получения текущей даты и времени в формате tm
const struct tm* getCurrentTime() {
    auto now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);
    return std::localtime(&now_c);
}

//Експорт данних в HTML-файл
std::string export_import::exportToHTML(ManageSubject &manager) {
    std::vector<Subject> allSubjects = manager.getAllSubjects();
    std::vector<SubjectWithCounters> exportList(allSubjects.size());

    for (const auto &subject : allSubjects) {
        exportList.push_back(ManageSubject::createSubjectWithCounters(subject, manager));
    }

    // Формирование имени файла с датой и временем
    char filename[100];
    std::strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S_ExportData_Subject.html", CURRENT_TIME);

    // Создаем HTML-файл и записываем в него данные
    std::ofstream file(DESKTOP_PATH + filename);
    if (file.is_open()) {
        file << "<html>\n";
        file << "<head>\n";
        file << "<title>Exported Subject Data</title>\n";
        file << "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'>\n";
        file << "</head>\n";
        file << "<body>\n";
        file << "<div class='container'>\n";
        file << "<table class='table table-striped'>\n";
        file << "<tr>\n";
        file << "<th>№</th>\n";
        file << "<th>Название предмета</th>\n";
        file << "<th>Количество вопросов</th>\n";
        file << "</tr>\n";

        for (size_t i = 0; i < exportList.size(); ++i) {
            file << "<tr";
            if (i % 2 == 0) {
                file << " class='table-light'";
            }
            file << ">\n";
            file << "<td>" << i + 1 << "</td>\n";
            file << "<td>" << exportList[i].subjectName << "</td>\n";
            file << "<td>" << exportList[i].testCount << "</td>\n";
            file << "</tr>\n";
        }

        file << "</table>\n";
        file << "</div>\n";
        file << "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js'></script>\n";
        file << "</body>\n";
        file << "</html>\n";
        file.close();
    } else {
        return "";
    }

    return DESKTOP_PATH + filename;
}

//Экспорт данных в файл JSON.
std::string export_import::exportToJson(ManageSubject &manager) {
    std::vector<Subject> allSubjects = manager.getAllSubjects();
    std::vector<SubjectWithCounters> exportList(allSubjects.size());

    for (const auto& subject : allSubjects) {
        exportList.push_back(ManageSubject::createSubjectWithCounters(subject, manager));
    }

    // Формирование имени файла с датой и временем
    char filename[100];
    std::strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S_ExportData_Subject.json", CURRENT_TIME);

    // Создаем JSON-массив для всех записей
    json ExportDataArray = json::array();

    for (const auto& subject : exportList) {
        json subjectData;
        subjectData["subject_id"] = subject.subjectID;
        subjectData["subject_name"] = subject.subjectName;
        subjectData["test_count"] = subject.testCount;
        ExportDataArray.push_back(subjectData);
    }

    // Записываем JSON-массив в файл
    std::ofstream file(DESKTOP_PATH + filename);
    file << ExportDataArray.dump(4);
    file.close();

    return DESKTOP_PATH + filename;
}

    // Метод для экспорта одного теста в JSON файл
    std::string export_import::exportTestToJson(const Test &test) {
        json testJson;
        testJson["testID"] = test.testID;
        testJson["correctOption"] = test.correctOption;
        testJson["score"] = test.score;
        testJson["subject"] = test.subject;
        testJson["questionText"] = test.questionText;
        testJson["option1"] = test.option1;
        testJson["option2"] = test.option2;
        testJson["option3"] = test.option3;

        char filename[100];
        std::strftime(filename, sizeof(filename), "%Y%m%d_%H%M%S_Test.json", CURRENT_TIME);

        std::ofstream file(DESKTOP_PATH + filename);
        if (file.is_open()) {
            file << testJson.dump(4);
            file.close();
            return DESKTOP_PATH + filename;
        } else {
            return "";
        }
    }

    // Метод для экспорта одного теста в HTML файл
    std::string export_import::exportTestToHTML(const Test &test) {
        json testJson;
        testJson["testID"] = test.testID;
        testJson["correctOption"] = test.correctOption;
        testJson["score"] = test.score;
        testJson["subject"] = test.subject;
        testJson["questionText"] = test.questionText;
        testJson["option1"] = test.option1;
        testJson["option2"] = test.option2;
        testJson["option3"] = test.option3;

        char filename[100];
        std::strftime(filename, sizeof(filename), "%Y%m%d_%H%M%S_Test.html", CURRENT_TIME);

        std::ofstream file(DESKTOP_PATH + filename);
        if (file.is_open()) {
            file << "<html>\n";
            file << "<head>\n";
            file << "<title>Test Data</title>\n";
            file << "</head>\n";
            file << "<body>\n";
            file << "<div class='container'>\n";
            file << "<table class='table table-striped'>\n";

            for (auto it = testJson.begin(); it != testJson.end(); ++it) {
                file << "<tr>\n";
                file << "<td>" << it.key() << "</td>\n";
                file << "<td>" << it.value() << "</td>\n";
                file << "</tr>\n";
            }

            file << "</table>\n";
            file << "</div>\n";
            file << "</body>\n";
            file << "</html>\n";
            file.close();
            return DESKTOP_PATH + filename;
        } else {
            return "";
        }
    }

    // Метод для экспорта всех тестов в JSON файл
    std::string export_import::exportAllTestsToJson(const std::vector<Test> &tests) {
        json testsJson;
        for (const auto &test : tests) {
            json testJson;
            testJson["testID"] = test.testID;
            testJson["correctOption"] = test.correctOption;
            testJson["score"] = test.score;
            testJson["subject"] = test.subject;
            testJson["questionText"] = test.questionText;
            testJson["option1"] = test.option1;
            testJson["option2"] = test.option2;
            testJson["option3"] = test.option3;
            testsJson.push_back(testJson);
        }



        char filename[100];
        std::strftime(filename, sizeof(filename), "%Y%m%d_%H%M%S_AllTests.json", CURRENT_TIME);

        std::ofstream file(DESKTOP_PATH + filename);
        if (file.is_open()) {
            file << testsJson.dump(4);
            file.close();
            return DESKTOP_PATH + filename;
        } else {
            return "";
        }
    }

    // Метод для экспорта всех тестов в HTML файл с таблицами для каждого теста
     std::string export_import::exportAllTestsToHTML(const std::vector<Test> &tests) {


        char filename[100];
        std::strftime(filename, sizeof(filename), "%Y%m%d_%H%M%S_AllTests.html", CURRENT_TIME);

        std::ofstream file(DESKTOP_PATH + filename);
        if (file.is_open()) {
            file << "<html>\n";
            file << "<head>\n";
            file << "<title>All Tests Data</title>\n";
            file << "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'>\n";
            file << "</head>\n";
            file << "<body>\n";
            file << "<div class='container'>\n";

            for (const auto &test : tests) {
                json testJson;
                testJson["testID"] = test.testID;
                testJson["correctOption"] = test.correctOption;
                testJson["score"] = test.score;
                testJson["subject"] = test.subject;
                testJson["questionText"] = test.questionText;
                testJson["option1"] = test.option1;
                testJson["option2"] = test.option2;
                testJson["option3"] = test.option3;

                file << "<table class='table table-striped'>\n";
                for (auto it = testJson.begin(); it != testJson.end(); ++it) {
                    file << "<tr>\n";
                    file << "<td>" << it.key() << "</td>\n";
                    file << "<td>" << it.value() << "</td>\n";
                    file << "</tr>\n";
                }
                file << "</table>\n";
            }

            file << "</div>\n";
            file << "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js'></script>\n";
            file << "</body>\n";
            file << "</html>\n";
            file.close();
            return DESKTOP_PATH + filename;
        } else {
            return "";
        }
    }