//
// Created by Denis Ryzhkov on 08.11.2023.
//
#pragma once
#ifndef CW1_EXPORT_IMPORT_H
#define CW1_EXPORT_IMPORT_H


#include <fstream>
#include <chrono>
#include <ctime>
#include "../controller/ManageSubject.h"
#include "../controller/ManageTest.h"

#define DESKTOP_PATH getDesktopPath()
#define CURRENT_TIME getCurrentTime()

class export_import {
public:
    static std::string exportToJson(ManageSubject &manager);
    static std::string exportToHTML(ManageSubject &manager);
    static std::string exportTestToJson(const Test &test);
    static std::string exportTestToHTML(const Test &test);
    static std::string exportAllTestsToJson(const std::vector<Test> &tests);
    static std::string exportAllTestsToHTML(const std::vector<Test> &tests);


};


#endif //CW1_EXPORT_IMPORT_H
