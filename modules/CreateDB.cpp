//
// Created by Denis Ryzhkov on 25.10.2023.
//
#include <iostream>
#include "../model/DataHandler.h"

class CreateDB{
    DataHandler &dbHandler;

public:
    CreateDB(DataHandler& dbHandler) : dbHandler(dbHandler){}


    bool create(){
        std::string createTableSQL = "CREATE TABLE IF NOT EXISTS users ("
                                     "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "login VARCHAR(20) NOT NULL, "
                                     "name VARCHAR(25) NOT NULL, "
                                     "address VARCHAR(50), "
                                     "phone VARCHAR(12), "
                                     "password VARCHAR(255), "
                                     "role INTEGER(2) NOT NULL);";



        std::string createResultsTableSQL = "CREATE TABLE IF NOT EXISTS test_results ("
                                            "result_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                            "test_date DATE, "
                                            "subject VARCHAR(50), "
                                            "score INTEGER, "
                                            "attempts INTEGER, "
                                            "user_id INTEGER, "
                                            "FOREIGN KEY (user_id) REFERENCES users(ID)"
                                            ");";

        std::string createTestsTableSQL = "CREATE TABLE IF NOT EXISTS tests ("
                                          "test_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                          "subject VARCHAR(50), "
                                          "question_text TEXT, "
                                          "option1 VARCHAR(255), "
                                          "option2 VARCHAR(255), "
                                          "option3 VARCHAR(255), "
                                          "correct_option INTEGER, "
                                          "score INTEGER, "
                                          "FOREIGN KEY (subject) REFERENCES subjects(subject_name)"
                                          ");";

        std::string createSubjectsTableSQL = "CREATE TABLE IF NOT EXISTS subjects ("
                                             "subject_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                             "subject_name VARCHAR(50) NOT NULL"
                                             ");";
        if (!dbHandler.executeNonQuery(createTableSQL)) {
            std::cerr << "Ошибка при создании таблицы: 'USERS'" << std::endl;
            return false;
        }
        if (!dbHandler.executeNonQuery(createResultsTableSQL)) {
            std::cerr << "Ошибка при создании таблицы: 'test_results'" << std::endl;
            return false;
        }
        if (!dbHandler.executeNonQuery(createTestsTableSQL)) {
            std::cerr << "Ошибка при создании таблицы: 'tests'" << std::endl;
            return false;
        }
        if (!dbHandler.executeNonQuery(createSubjectsTableSQL)) {
            std::cerr << "Ошибка при создании таблицы: 'subjects'" << std::endl;
            return false;
        }
        return true;
    }


};