//
// Created by Denis Ryzhkov on 25.10.2023.
//

#include "DataHandler.h"


DataHandler::DataHandler(DataBase& db) : database(db) {}

bool DataHandler::executeNonQuery(const std::string& sql) {
    sqlite3* db = database.getDBConnection();
    char* errMsg = nullptr;
    int result = sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &errMsg);

    if (result != SQLITE_OK) {
        sqlite3_free(errMsg);
        return false;
    }

    return true;
}


std::vector<std::vector<std::string>> DataHandler::executeQuery(const std::string& query) {
    sqlite3* db = database.getDBConnection();
    sqlite3_stmt* statement;
    int result = sqlite3_prepare_v2(db, query.c_str(), -1, &statement, nullptr);

    std::vector<std::vector<std::string>> results;

    if (result == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int columns = sqlite3_column_count(statement);
            std::vector<std::string> row;

            for (int i = 0; i < columns; i++) {
                const char* text = reinterpret_cast<const char*>(sqlite3_column_text(statement, i));
                if (text) {
                    row.push_back(text);
                } else {
                    row.push_back(""); // Если значение NULL
                }
            }

            results.push_back(row);
        }

        sqlite3_finalize(statement);
    }

    return results;
}

//дополнительный вариант чтения данных из базы данных в виде массива vector<map<string, string>>
//тестирование использования для упрощения обработки полученых данных

std::vector<std::map<std::string, std::string>> DataHandler::executeQueryMap(const std::string& query) {
    sqlite3* db = database.getDBConnection();
    sqlite3_stmt* statement;
    int result = sqlite3_prepare_v2(db, query.c_str(), -1, &statement, nullptr);

    std::vector<std::map<std::string, std::string>> results;

    if (result == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int columns = sqlite3_column_count(statement);
            std::map<std::string, std::string> row;

            for (int i = 0; i < columns; i++) {
                const char* columnName = sqlite3_column_name(statement, i);
                const char* text = reinterpret_cast<const char*>(sqlite3_column_text(statement, i));
                if (columnName && text) {
                    row[std::string(columnName)] = text;
                }
            }

            results.push_back(row);
        }

        sqlite3_finalize(statement);
    }

    return results;
}



std::string DataHandler::escapeString(const std::string& input) {
    std::string output;
    for (char c : input) {
        if (c == '\'') {
            output += "''";
        } else {
            output += c;
        }
    }
    return output;
}