//
// Created by Denis Ryzhkov on 25.10.2023.
//

#pragma once
#ifndef CW1_DATAHANDLER_H
#define CW1_DATAHANDLER_H

#include <string>
#include <vector>
#include <map>

#include "DataBase.h"
#include "../modules/json.hpp"



#ifdef HAVE_SQLITE3
#include <sqlite3.h>
#endif



using json = nlohmann::json;


class DataHandler {
    DataBase& database;
public:
    DataHandler(DataBase& db);

    bool executeNonQuery(const std::string &query);
    std::vector<std::vector<std::string>> executeQuery(const std::string& query);
    std::string escapeString(const std::string& input);


    std::vector<std::map<std::string, std::string>> executeQueryMap(const std::string &query);
};

#endif //CW1_DATAHANDLER_H
