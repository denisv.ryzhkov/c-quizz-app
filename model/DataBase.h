//
// Created by Denis Ryzhkov on 25.10.2023.
//
#pragma once
#ifndef CW1_DATABASE_H
#define CW1_DATABASE_H

#include "sqlite3.h"
#include <string>

class DataBase {
    std::string dbName;
    sqlite3* db;
public:
    DataBase(std::string  dbFileName);
    ~DataBase();

    bool open();
    bool close();

    sqlite3* getDBConnection();

};


#endif //CW1_DATABASE_H
