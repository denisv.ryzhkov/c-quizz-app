//
// Created by Denis Ryzhkov on 25.10.2023.
//

#include "DataBase.h"

#include <utility>

DataBase::DataBase(std::string  dbFileName) : dbName(std::move(dbFileName)), db(nullptr) {}

DataBase::~DataBase() {
    if (db) {
        close();
    }
}

bool DataBase::open() {
    int result = sqlite3_open(dbName.c_str(), &db);
    return (result == SQLITE_OK);
}

bool DataBase::close() {
    if (db) {
        int result = sqlite3_close(db);
        db = nullptr;
        return (result == SQLITE_OK);
    }
    return false;
}

sqlite3* DataBase::getDBConnection() {
    return db;
}