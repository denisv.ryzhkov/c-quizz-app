//
// Created by Denis Ryzhkov on 25.10.2023.
//

#ifndef CW1_MANAGETEST_H
#define CW1_MANAGETEST_H

#include "../model/DataHandler.h"

struct Test {
    int testID;
    int correctOption;
    int score;
    std::string subject;
    std::string questionText;
    std::string option1;
    std::string option2;
    std::string option3;
};

class ManageTest {
private:
    DataHandler& dbHandler;
public:
    ManageTest(DataHandler& dbHandler) : dbHandler(dbHandler) {}

    bool createTest(const Test& test);

    bool editTest(int testID, const Test& test);

    bool deleteTest(int testID);

    std::vector<Test> getTestsBySubject(const std::string& subject);


};


#endif //CW1_MANAGETEST_H
