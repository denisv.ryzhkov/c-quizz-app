//
// Created by Denis Ryzhkov on 25.10.2023.
//

#include "ManageTestResult.h"


bool ManageTestResult::addTestResult(const TestResult &result) {
    std::string query = "INSERT INTO test_results (test_date, subject, score, attempts, user_id) VALUES "
                        "('" + dbHandler.escapeString(result.testDate) + "', '" +
                        dbHandler.escapeString(result.subject) + "', " +
                        std::to_string(result.score) + ", " +
                        std::to_string(result.attempts) + ", " +
                        std::to_string(result.userID) + ");";

    return dbHandler.executeNonQuery(query);
}

std::vector<TestResult> ManageTestResult::getTestResultsByUserID(int userID) {
    std::vector<TestResult> results;
    std::string query = "SELECT * FROM test_results WHERE user_id = " + std::to_string(userID) + ";";
    std::vector<std::vector<std::string>> rows = dbHandler.executeQuery(query);

    for (const auto &row : rows) {
        if (row.size() == 6) {
            TestResult result;
            result.resultID = std::stoi(row[0]);
            result.testDate = row[1];
            result.subject = row[2];
            result.score = std::stoi(row[3]);
            result.attempts = std::stoi(row[4]);
            result.userID = std::stoi(row[5]);
            results.push_back(result);
        }
    }

    return results;
}