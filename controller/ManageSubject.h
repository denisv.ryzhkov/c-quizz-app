//
// Created by Denis Ryzhkov on 25.10.2023.
//

#ifndef CW1_MANAGESUBJECT_H
#define CW1_MANAGESUBJECT_H


#include "../model/DataHandler.h"




struct Subject {
    int subjectID;
    std::string subjectName;
};

struct SubjectWithCounters: public Subject
{
    int testCount;
};

class ManageSubject {
private:
    DataHandler &dbHandler;
public:
    ManageSubject(DataHandler &dbHandler) : dbHandler(dbHandler) {}

    // Добавить новый предмет.
    bool addSubject(const std::string &subjectName);

    // Удалить предмет по ID.
    bool deleteSubject(int subjectID);

    // Получить предмет по ID.
    Subject getSubjectByID(int subjectID);

    // Получить все предметы.
    std::vector<Subject> getAllSubjects();

    // Редактировать предмет.
    bool editSubject(int subjectID, const std::string &newSubjectName);

    int getTestCountBySubject(const std::string &subject);
    static SubjectWithCounters createSubjectWithCounters(const Subject &subject, ManageSubject &manager);

};

#endif //CW1_MANAGESUBJECT_H
