//
// Created by Denis Ryzhkov on 25.10.2023.
//

#ifndef CW1_MANAGETESTRESULT_H
#define CW1_MANAGETESTRESULT_H

#include "../model/DataHandler.h"

struct TestResult {
    int resultID;
    int score;
    int attempts;
    int userID;
    std::string testDate;
    std::string subject;
};


class ManageTestResult {
private:
    DataHandler &dbHandler;
public:
    ManageTestResult(DataHandler &dbHandler) : dbHandler(dbHandler) {}

    bool addTestResult(const TestResult &result);
    std::vector<TestResult> getTestResultsByUserID(int userID);

};

#endif //CW1_MANAGETESTRESULT_H
