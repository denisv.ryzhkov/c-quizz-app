//
// Created by Denis Ryzhkov on 25.10.2023.
//

#include "ManageUsers.h"
#include <string>
#include <sstream>


// Функция для хеширования пароля
std::string ManageUsers::hashPassword(const std::string &password) {
    // Используем стандартную хеш-функцию std::hash
    std::hash<std::string> hash_fn;
    size_t hashedValue = hash_fn(password);

    // Преобразуем числовой хеш в строку
    std::stringstream stream;
    stream << std::hex << hashedValue;
    return stream.str();
}

bool ManageUsers::isLoginUnique(const std::string &login) {
    // Проверка на уникальность логина
    std::string checkQuery = "SELECT * FROM users WHERE login = '" + dbHandler.escapeString(login) + "';";
    std::vector<std::vector<std::string>> existingUser = dbHandler.executeQuery(checkQuery);

    return existingUser.empty();
}

bool ManageUsers::createUser(const User &user) {

    std::string passwordHash = hashPassword(user.password);
    std::string insertQuery = "INSERT INTO users (login, name, address, phone, password, role) VALUES ('" +
            dbHandler.escapeString(user.login) + "', '" + dbHandler.escapeString(user.name) + "', '" +
            dbHandler.escapeString(user.address) + "', '" + dbHandler.escapeString(user.phone) + "', '" +
            passwordHash + "', " + std::to_string(user.role) + ");";

    return dbHandler.executeNonQuery(insertQuery);
}

User ManageUsers::login(const std::string &login, const std::string &password) {
    User user;
    user.isAuthenticated = false;

    std::string passwordHash = hashPassword(password);
    std::string query = "SELECT * FROM users WHERE login = '" +
            dbHandler.escapeString(login) + "' AND password = '" + passwordHash + "';";

    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(query);
    if (!result.empty()) {
        user.userID = std::stoi(result[0][0]);
        user.login = result[0][1];
        user.name = result[0][2];
        user.address = result[0][3];
        user.phone = result[0][4];
        user.password = result[0][5];
        user.role = std::stoi(result[0][6]);
        user.isAuthenticated = true;
    }

    return user;
}

void ManageUsers::logout(User &user) {
    user.isAuthenticated = false;
}

bool ManageUsers::editUser(const User &user) {
    std::string passwordHash = hashPassword(user.password);
    std::string updateQuery = "UPDATE users SET name = '" + dbHandler.escapeString(user.name) + "', address = '" +
            dbHandler.escapeString(user.address) + "', phone = '" +
            dbHandler.escapeString(user.phone) + "', password = '" + passwordHash + "', role = " +
            std::to_string(user.role) + " WHERE ID = " + std::to_string(user.userID) + ";";

    return dbHandler.executeNonQuery(updateQuery);
}

bool ManageUsers::deleteUser(int userID) {
    std::string deleteQuery = "DELETE FROM users WHERE ID = " + std::to_string(userID) + ";";
    return dbHandler.executeNonQuery(deleteQuery);
}

std::vector<User> ManageUsers::getAllUsers() {
    std::vector<User> users;

    std::string selectQuery = "SELECT * FROM users";
    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(selectQuery);

    // Маппинг результата запроса на объект Subject.
    for (const std::vector<std::string> &row : result) {
            User user;
            user.userID = std::stoi(row[0]);
            user.login = row[1];
            user.name = row[2];
            user.address = row[3];
            user.phone = row[4];
            users.push_back(user);
    }

    return users;
}
//дополнительный вариант чтения данных из базы данных в виде массива vector<map<string, string>>
//тестирование использования для упрощения обработки полученых данных
//практический смысл в данном варианте является тем, что заполнение структуры данных происходит помощью доступа по ключу
std::vector<User> ManageUsers::getAllUsersMap() {
    std::vector<User> users;

    std::string selectQuery = "SELECT * FROM users";
    std::vector<std::map<std::string, std::string>> result = dbHandler.executeQueryMap(selectQuery);

    // Маппинг результата запроса на объект User.
    for (const std::map<std::string, std::string> &row : result) {
        User user;
        user.userID = std::stoi(row.at("userID"));
        user.login = row.at("login");
        user.name = row.at("name");
        user.address = row.at("address");
        user.phone = row.at("phone");
        users.push_back(user);
    }

    return users;
}

User ManageUsers::getUserByID(int userID) {
    User user;

    std::string selectQuery = "SELECT * FROM users WHERE ID = " + std::to_string(userID);
    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(selectQuery);

    if (!result.empty()) {
        // Маппинг результата запроса на объект User (первая строка результата).
        const std::vector<std::string> &row = result[0];
        user.userID = std::stoi(row[0]);
        user.login = row[1];
        user.name = row[2];
        user.address = row[3];
        user.phone = row[4];
    }

    return user;
}

int ManageUsers::GetUsersCount() {
    std::string countQuery = "SELECT COUNT(*) FROM users";
    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(countQuery);

    if (!result.empty() && result[0].size() == 1) {
        return std::stoi(result[0][0]);
    } else {
        return -1;
    }
}


