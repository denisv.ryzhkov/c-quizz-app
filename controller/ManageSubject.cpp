//
// Created by Denis Ryzhkov on 25.10.2023.
//

#include "ManageSubject.h"


bool ManageSubject::addSubject(const std::string &subjectName){
        std::string insertQuery = "INSERT INTO subjects (subject_name) VALUES ('" + dbHandler.escapeString(subjectName) + "')";
        return dbHandler.executeNonQuery(insertQuery);
}

bool ManageSubject::deleteSubject(int subjectID) {
    std::string deleteQuery = "DELETE FROM subjects WHERE subject_id = " + std::to_string(subjectID);
    return dbHandler.executeNonQuery(deleteQuery);
}

Subject ManageSubject::getSubjectByID(int subjectID) {

    std::string selectQuery = "SELECT subject_id, subject_name FROM subjects WHERE subject_id = " + std::to_string(subjectID);

    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(selectQuery);
    if (result.size() == 1 && result[0].size() == 2) {
        Subject subject;
        subject.subjectID = std::stoi(result[0][0]);
        subject.subjectName = result[0][1];
        return subject;
    } else {
        return {};
    }
}

//Редактирование предмета
bool ManageSubject::editSubject(int subjectID, const std::string &newSubjectName) {
    std::string updateQuery = "UPDATE subjects SET subject_name = '" + dbHandler.escapeString(newSubjectName) + "' WHERE subject_id = " + std::to_string(subjectID);
    return dbHandler.executeNonQuery(updateQuery);
}

//Получение всех предметов
std::vector<Subject> ManageSubject::getAllSubjects() {
    std::vector<Subject> subjects;

    // Подготовьте SQL-запрос для выбора всех предметов.
    std::string selectQuery = "SELECT subject_id, subject_name FROM subjects";
    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(selectQuery);

    // Маппинг результата запроса на объект Subject.
    for (const std::vector<std::string> &row : result) {
        if (row.size() == 2) {
            Subject subject;
            subject.subjectID = std::stoi(row[0]);
            subject.subjectName = row[1];
            subjects.push_back(subject);
        }
    }

    return subjects;
}
//Получение количества тестов для каждого предмета
int ManageSubject::getTestCountBySubject(const std::string &subject) {
    std::string selectQuery = "SELECT COUNT(*) FROM tests WHERE subject = '" + dbHandler.escapeString(subject) + "'";

    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(selectQuery);

    if (result.size() == 1 && result[0].size() == 1) {
        return std::stoi(result[0][0]); // Возвращает количество найденных тестов.
    } else {
        return 0; // Возвращает 0, если не удалось получить количество тестов.
    }
}

// Функция для преобразования Subject в SubjectWithCounters
SubjectWithCounters ManageSubject::createSubjectWithCounters(const Subject &subject, ManageSubject &manager) {
    SubjectWithCounters result;
    result.subjectName = subject.subjectName;
    result.subjectID = subject.subjectID;
    result.testCount = manager.getTestCountBySubject(subject.subjectName);

    return result;
}

