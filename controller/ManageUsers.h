//
// Created by Denis Ryzhkov on 25.10.2023.
//

#ifndef CW1_MANAGEUSERS_H
#define CW1_MANAGEUSERS_H

#include "../model/DataHandler.h"

struct User {
    bool isAuthenticated; // Статус авторизации
    int role;
    int userID;
    std::string login;
    std::string name;
    std::string address;
    std::string phone;
    std::string password;

};

class ManageUsers {
    DataHandler &dbHandler;

    std::string hashPassword(const std::string &password);
public:
    ManageUsers(DataHandler &dbHandler): dbHandler(dbHandler) {}

    bool createUser(const User &user);
    User login(const std::string &login, const std::string &password);
    void logout(User &user);
    bool editUser(const User &user);
    bool deleteUser(int userID);


    bool isLoginUnique(const std::string &login);

    std::vector<User> getAllUsers();
    std::vector<User> getAllUsersMap();

    int GetUsersCount();

    User getUserByID(int userID);
};


#endif //CW1_MANAGEUSERS_H
