//
// Created by Denis Ryzhkov on 25.10.2023.
//

#include "ManageTest.h"

bool ManageTest::createTest(const Test &test)  {

    std::string subject = dbHandler.escapeString(test.subject);
    std::string questionText = dbHandler.escapeString(test.questionText);
    std::string option1 = dbHandler.escapeString(test.option1);
    std::string option2 = dbHandler.escapeString(test.option2);
    std::string option3 = dbHandler.escapeString(test.option3);


    std::string insertQuery = "INSERT INTO tests (subject, questionText, option1, option2, option3, correctOption, score) "
                              "VALUES ('" + subject + "', '" + questionText + "', '" + option1 + "', '"
                              + option2 + "', '" + option3 + "', " + std::to_string(test.correctOption) + ", " + std::to_string(test.score) + ");";

    return dbHandler.executeNonQuery(insertQuery);
}


bool ManageTest::editTest(int testID, const Test &test) {
    std::string updateQuery = "UPDATE tests SET "
    "subject = '" + dbHandler.escapeString(test.subject) + "', "
    "question_text = '" + dbHandler.escapeString(test.questionText) + "', "
    "option1 = '" + dbHandler.escapeString(test.option1) + "', "
    "option2 = '" + dbHandler.escapeString(test.option2) + "', "
    "option3 = '" + dbHandler.escapeString(test.option3) + "', "
    "correct_option = " + std::to_string(test.correctOption) + ", "
    "score = " + std::to_string(test.score) + " "
    "WHERE test_id = " + std::to_string(testID);

    return dbHandler.executeNonQuery(updateQuery);
}

bool ManageTest::deleteTest(int testID) {
    std::string deleteQuery = "DELETE FROM tests WHERE test_id = " + std::to_string(testID);

    return dbHandler.executeNonQuery(deleteQuery);
}


std::vector<Test> ManageTest::getTestsBySubject(const std::string &subject) {
    std::vector<Test> tests;

    std::string selectQuery = "SELECT test_id, subject, question_text, option1, option2, option3, correct_option, score FROM tests WHERE subject = '" + dbHandler.escapeString(subject) + "'";

    std::vector<std::vector<std::string>> result = dbHandler.executeQuery(selectQuery);

    // Маппинг результата запроса на объекты Test.
    for (const std::vector<std::string> &row : result) {
        Test test;
        if (row.size() >= 8) {
            test.testID = std::stoi(row[0]);
            test.subject = row[1];
            test.questionText = row[2];
            test.option1 = row[3];
            test.option2 = row[4];
            test.option3 = row[5];
            test.correctOption = std::stoi(row[6]);
            test.score = std::stoi(row[7]);
            tests.push_back(test);
        }
    }

    return tests;
}


